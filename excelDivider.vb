Sub getUniqueValues()

    ' user should select the only cell
    If Selection.Count > 1 Then
        MsgBox "more than one! please, select the only cell of a column to sort by unique values. Pay attention to the following: The sheet must be named as Sheet1"
        Exit Sub
    Else
        MsgBox "Want to Continue?", vbOKCancel
        If vbOK = 1 Then
            Call handlerValues
        Else
            Exit Sub
        End If
    End If
    
    ' we close the last active workbook called "Reserved", cause there's no more students in values left
    ' and also we unselect cells
    ActiveWorkbook.Close
    ActiveWorkbook.ActiveSheet.ShowAllData
End Sub

Function handlerValues()
    ' creating of new cells of an array is a kind of slow operations in Visual Basic so we create 10000 of them at the moment
    Dim ValuesToSave(100000) As String
    Dim lo As ListObject
    Dim Counter As Integer
    Dim Path As String
    Dim Cel1 As Range
    
    ' select from current active cell to the bottom of the table.
	' TODO - 1: make it universal. filter goes from an active cell
    Range(Selection, Cells(Rows.Count, 2).End(xlUp)).Select
    Path = Application.ActiveWorkbook.Path
    Set Worksheet = ActiveWorkbook.ActiveSheet
    Set lo = Worksheet.ListObjects(1)
    
    ' save unique values of our table
    Counter = 0
    For Each Value In UniqueValues(Selection.Value)
        ValuesToSave(Counter) = Value
        Counter = Counter + 1
    Next
    
    ' sort data by unique values from the array ValuesToSave, copy data, create new Work Sheet and paste data into it, then make autofit.
    ' if values contain nothing then we should save it and call the following data by "Reserved". In other cases we call files other way.
    '
    For i = 0 To UBound(ValuesToSave)
		' TODO - 1: make it universal. filter goes from an active cell
        lo.Range.AutoFilter Field:=2, Criteria1:=ValuesToSave(i)
        lo.AutoFilter.Range.SpecialCells(xlCellTypeVisible).Copy
        Workbooks.Add.Worksheets(1).Paste
        Cells.EntireColumn.AutoFit
        If ValuesToSave(i) = "" Then
            ActiveWorkbook.SaveAs Filename:="Reserved"
            Exit Function
        Else
            ActiveWorkbook.SaveAs Filename:=ValuesToSave(i)
        End If
        ActiveWorkbook.Close
    Next
End Function

Function UniqueValues(ByVal arr) As Collection
    Set UniqueValues = New Collection: On Error Resume Next
    For Each v In arr
        v = Trim(v): If Len(v) Then UniqueValues.Add CStr(v), CStr(v)
    Next v
End Function
